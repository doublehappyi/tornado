__author__ = 'db'

import os.path

import tornado.httpserver
import tornado.ioloop
import tornado.web
import tornado.options

from tornado.options import define, options

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
#print "BASE_DIR == " + str(BASE_DIR)

define("port", default=8000, help="this port default is 8000", type=int)


class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        # self.write("hello index page !")
        self.render("index/index.html")


if __name__ == "__main__":
    tornado.options.parse_command_line()
    app = tornado.web.Application(
        handlers=[(r"/", IndexHandler)],
        template_path=os.path.join(BASE_DIR, "front", "templates"),
        static_path=os.path.join(BASE_DIR, "front", "static"),
        debug=True
    )
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()