__author__ = 'db'

from tornado.concurrent import Future
from tornado.httpclient import AsyncHTTPClient

def async_fetch_future(url):
    http = AsyncHTTPClient()
    my_future = Future()
    fetch_future = http.fetch(url)
    fetch_future.add_done_callback(
        lambda f:my_future.set_result(f.body)
    )

    return my_future

my = async_fetch_future("http://www.baidu.com")