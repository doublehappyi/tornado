from tornado import gen
from tornado.httpclient import AsyncHTTPClient

@gen.coroutine
def fetch_coroutine(url):
  http = AsyncHTTPClient()
  response = yield http.fetch(url)
  raise gen.Return(response.body)

fetch = fetch_coroutine("https://www.baidu.com")


def show_response(res):
    print "show response ..."
    print res.body

@gen.coroutine
def fetch_task(url):
  http = AsyncHTTPClient()
  response = yield http.fetch(url)
  raise gen.Task(show_response, response)


f = fetch_task()

