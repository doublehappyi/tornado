/**
 * Created by db on 14-11-13.
 */
$(function () {
    $("#submit").click(function () {
        var data = {
            "id": $(this).attr("data-id"),
            "type": 2,
            "response": $("#response").val(),
            "_xsrf": $.cookie("_xsrf")
        }
        $.ajax("/comment", {
            data: data,
            type: "POST",
            success: function (data) {
                if (data.success) {
                    var comment = data.data.comment,
                        user = comment.user;
                    $('<div class="clear res">' +
                        '<div class="fl res-image">' +
                        '<a href="/user/' + user._id + '">' +
                        '<img class="user-image" src="' + user.image + '">' +
                        '</a>' +
                        '</div>' +
                        '<div class="fl res-content">' +
                        '<a href="/user/' + user._id + '">' +
                        '<span style="color: #2a6496">' + user.username + '</span>' +
                        '</a>' +

                        '<div>' + comment.response + '</div>' +
                        '</div>' +
                        '<span class="fr res-floor" style="color: #ccc;width: 10%;">'+data.data.count+'楼</span>' +
                        '</div>' +
                        '<div style="text-align: right;color: #ccc;"><span>' + comment.date + '</span></div>' +
                        '<hr/>').appendTo($("#response-container"));

                    $("#response").val("")
                }
            },
            error: function () {
            }
        })
    });
});