/**
 * Created by db on 14-11-6.
 */
$(function () {
    bindEvents();
    function bindEvents() {
        commentBind($(".good"), 1);
        commentBind($(".bad"), 0);
        addFocus();
    }

    function commentBind($dom, type, cb) {
        comment_url = "/comment";
        $dom.click(function () {
            var $this = $(this),
                id = $this.attr("data-id");
            $.ajax(comment_url, {
                type: "POST",
                data: {
                    type: type,
                    id: id,
                    _xsrf: $.cookie("_xsrf")
                },
                success: function (data) {
                    console.log(data);
                    if (data.success) {
                        $this.next("span").text(data.data.count);
                    }
                },
                error: function () {

                }
            });
        });
    }

    function addFocus() {
        $(".j-focus").click(function () {
            var $this = $(this);
            var data = {
                _xsrf: $.cookie("_xsrf"),
                type: 1,
                id: $this.attr("data-id")
            }
            $.ajax("/focus", {
                type: "POST",
                data: data,
                success: function (data) {
                    if (data.success || data.focusing) {
                        $this.unbind("click").text("已关注");
                    }
                }
            })
        });
    }
});