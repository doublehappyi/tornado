__author__ = 'db'
#coding=utf-8
import tornado.web

from pymongo import MongoClient, DESCENDING, ASCENDING
import settings
import datetime
import json
from bson import ObjectId


class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        return json.JSONEncoder.default(self, o)


class BaseHandler(tornado.web.RequestHandler):
    def get_current_user(self):
        return self.get_secure_cookie("username")

    def get_user(self):
        db = MongoClient(settings.mdb_server, settings.mdb_port)
        livebk = db.livebk
        user = livebk.user.find_one({"username": self.get_secure_cookie("username")})
        return user

    def get_livebk(self):
        db = MongoClient(settings.mdb_server, settings.mdb_port)
        livebk = db.livebk
        return livebk

    def now(self):
        return datetime.datetime.now()


    MONGO_DESCENDING = DESCENDING
    MONGO_ASCENDING = ASCENDING

    def get_json(self, mongodb_object):
        return json.loads(JSONEncoder().encode(mongodb_object), encoding="UTF-8")


class BaseLiveHandler(BaseHandler):
    def wrapLive(self, live_cursor, db):
        live = {}
        if self.get_user() and db.focus.find_one({"master": self.get_user()["_id"], "guest": live_cursor["user"]}) is not None:
            live["focus"] = True
        else:
            live["focus"] = False

        # 首先把user从id改成user对象
        for k in live_cursor.keys():
            if k == "user":
                live[k] = db.user.find_one({"_id": live_cursor[k]})
            else:
                live[k] = live_cursor[k]

        live["good_count"] = db.comment.find({"live": live["_id"], "type": 1}).count()
        live["bad_count"] = db.comment.find({"live": live["_id"], "type": 0}).count()
        live["response_count"] = db.comment.find({"live": live["_id"], "type": 2}).count()

        return live