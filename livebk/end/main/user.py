__author__ = 'db'
# encoding=utf-8
import base
from bson.objectid import ObjectId
import tornado.web
import os.path
import datetime
import re
import settings


class RegisterHandler(base.BaseHandler):
    def get(self):
        self.render("register.html")

    def post(self, *args, **kwargs):
        username = self.get_argument("username")
        password = self.get_argument("password")
        password_again = self.get_argument("password_again")
        email = self.get_argument("email")
        agree = self.get_argument("agree", "false")

        if agree != "true":
            self.write("您尚未同意《用户协议》及《隐私政策》！")
            return

        print "agree is legal"

        if password != password_again:
            self.write("您输入的密码不一致！")
            return

        print "password is legal"

        livebk = self.get_livebk()

        if livebk.user.find_one({"username": username}) is not None:
            self.write("用户名已经被注册！")
            return

        print "username is legal"

        if livebk.user.find_one({"email": email}) is not None:
            self.write("邮箱已经被注册！")
            return

        print "email is legal"

        object_id = livebk.user.insert({
            "username": username,
            "password": password,
            "email": email,
            "image": "/static/avatars/avatar.jpg"
        })

        if object_id is not None:
            self.write("注册成功")


class UserHandler(base.BaseLiveHandler):
    def get(self, _id):
        db = self.get_livebk()
        target_user = db.user.find_one({"_id": ObjectId(_id)})

        lives_cursor = db.live.find({"user": ObjectId(_id)}).sort("date", self.MONGO_DESCENDING)
        lives = []

        for l in lives_cursor:
            live = self.wrapLive(l, db)
            lives.append(live)

        focusing_count = db.focus.find({"master": ObjectId(_id)}).count()
        focused_count = db.focus.find({"guest": ObjectId(_id)}).count()
        target_user["focusing_count"] = focusing_count
        target_user["focused_count"] = focused_count
        self.render("user.html", user=self.get_user(), target_user=target_user, lives=lives)


class UserCommentHandler(base.BaseHandler):
    def get(self, _id, **kwargs):
        db = self.get_livebk()
        target_user = db.user.find_one({"_id": ObjectId(_id)})
        lives = []
        live_cursor = db.live.find()
        # 首先遍历所有live数据
        for live in live_cursor:
            # 取出live中的response数据
            response = live["response"]
            # 遍历response数据，如果发现response数据里面的user的值和当前的_id值相等，则这条live数据是该用户评论的
            for r in response:
                r_cursor = db.response.find_one({"_id": r})
                if str(r_cursor["user"]) == _id:
                    curr_live_cursor = db.live.find({"_id": r_cursor["live"]})
                    for curr_r in curr_live_cursor["response"]:
                        pass
                    lives.append()

                    break

        lives = db.live.find({"user": target_user["_id"]}).sort("photo_name", self.MONGO_DESCENDING)
        self.render("user-comment.html", user=self.get_user(), target_user=target_user, lives=lives)


class UserReplyHandler(base.BaseHandler):
    def get(self, _id, **kwargs):
        livebk = self.get_livebk()
        target_user = livebk.user.find_one({"_id": ObjectId(_id)})
        lives = livebk.live.find({"user": target_user["_id"]}).sort("photo_name", self.MONGO_DESCENDING)
        self.render("user-reply.html", user=self.get_user(), target_user=target_user, lives=lives)


class LoginHandler(base.BaseHandler):
    def get(self):
        self.render("login.html")

    def post(self, *args, **kwargs):
        username = self.get_argument("username")
        password = self.get_argument("password")
        next = self.get_argument("next", "/")

        print "next : " + next

        livebk = self.get_livebk()

        if livebk.user.find_one({"username": username, "password": password}) is not None:
            self.set_secure_cookie("username", username)
            self.redirect(next)
        else:
            self.write("登录失败，用户名或密码错误！")


class LogoutHandler(base.BaseHandler):
    def post(self):
        self.clear_cookie("username")
        self.redirect("/")


class ModifyHandler(base.BaseHandler):
    @tornado.web.authenticated
    def get(self):
        user = self.get_user()
        self.render("modify.html", user=user, target_user=user)


class ModifyAvatarHandler(base.BaseHandler):
    @tornado.web.authenticated
    def post(self):
        avatar = self.request.files['avatar']
        uploadpath = os.path.join(settings.BASE_DIR, 'front', 'static', 'avatars')

        for meta in avatar:
            file_name = meta["filename"]
            file = meta['body']
        timestamp = re.compile("[\.:\- ]").sub("", str(datetime.datetime.now()))
        avatar_name = timestamp + "_" + file_name
        file_path = os.path.join(uploadpath, avatar_name)

        livebk = self.get_livebk()

        # livebk.user.update({"username", self.get_secure_cookie("username")}, {"$set", {"avatar": avatar_name}})

        livebk.user.update({"username": self.get_secure_cookie("username")},
                           {"$set": {"image": "/static/avatars/" + avatar_name}})

        with open(file_path, 'wb') as up:
            up.write(file)
            self.redirect("/modify")


class ModifyPasswordHandler(base.BaseHandler):
    @tornado.web.authenticated
    def post(self):
        password_old = self.get_argument("password_old")
        password = self.get_argument('password')
        password_again = self.get_argument("password_again")

        livebk = self.get_livebk()
        username = self.get_secure_cookie("username")

        old_user = livebk.user.find_one({
            "username": username,
            "password": password_old
        })

        if password_old == password:
            self.write("新密码不能和当前密码相同")

        if password != password_again:
            self.write("您2次输入的密码不一致")
            return
        if old_user is not None:
            print "正在修改密码..."
            # livebk.user.udpate({"username": username}, {"$set": {"password": password}})
            livebk.user.update({"username": username}, {"$set": {"password": password}})
            self.clear_cookie("username")
            self.redirect("/modify")
        else:
            self.write("当前密码错误")


class ModifyEmailHandler(base.BaseHandler):
    @tornado.web.authenticated
    def post(self):
        email = self.get_argument("email")
        livebk = self.get_livebk()
        username = self.get_secure_cookie("username")
        livebk.user.update({"username": username}, {"$set": {"email": email}})
        self.redirect("/modify")


class FocusHandler(base.BaseHandler):
    def post(self, *args, **kwargs):
        _guest_id = ObjectId(self.get_argument("id"))
        _type = int(self.get_argument("type"))

        if self.get_user() is None:
            self.write({"success": False, "msg": "您还未登录"})
            return

        _user_id = self.get_user()["_id"]

        if _user_id == _guest_id:
            self.write({
                "success": False,
                "msg": "您不能关注您自己"
            })
            return

        db = self.get_livebk()

        if _type == 1:
            if db.focus.find_one({"master": _user_id, "guest": _guest_id}) is not None:
                self.write({
                    "success": False,
                    "focusing": True,
                    "msg": "您已经关注该用户"
                })
                return
            # master：关注别人
            # guest:被关注
            db.focus.insert({
                "date": datetime.datetime.now(),
                "master": _user_id,
                "guest": _guest_id
            })
            self.write({
                "success": True
            })