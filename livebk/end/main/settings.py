__author__ = 'db'

import os.path

import user
import index
import live

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

settings = {
    "template_path": os.path.join(BASE_DIR, "front", "templates"),
    "static_path": os.path.join(BASE_DIR, "front", "static"),
    "debug": True,
    "cookie_secret": "EOFsnUQ/SeG2V39seKNw0hwYJSevqEjxs/+lMFfHSd4=",
    "xsrf_cookies": True,
    "login_url": "/login"

}

handlers = [
    (r"/", index.IndexHandler),
    (r"/register", user.RegisterHandler),
    (r"/user", user.UserHandler),
    (r"/login", user.LoginHandler),
    (r"/logout", user.LogoutHandler),
    (r"/submit", live.SubmitHandler),
    (r"/live/(\w+)", live.LiveHandler),
    (r"/comment", live.CommentHandler),
    (r"/response/(\w+)", live.ResponseHandler),
    (r"/user/(\w+)", user.UserHandler),
    (r"/user/(\w+)/comment", user.UserCommentHandler),
    (r"/user/(\w+)/reply", user.UserReplyHandler),
    (r"/modify", user.ModifyHandler),
    (r"/modify/avatar", user.ModifyAvatarHandler),
    (r"/modify/email", user.ModifyEmailHandler),
    (r"/modify/password", user.ModifyPasswordHandler),
    (r"/focus", user.FocusHandler)
]

# template_path = os.path.join(BASE_DIR, "front", "templates")
#
# static_path = os.path.join(BASE_DIR, "front", "static")
#
# debug = True

mdb_server = "localhost"

mdb_port = 27017
