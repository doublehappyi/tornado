__author__ = 'db'
# coding=utf-8
import base
from bson import ObjectId

# class IndexHandler(BaseHandler):
# def get(self, *args, **kwargs):
# livebk = self.get_livebk()
# # 从数据库里面查询出来的列表是一个迭代列表，只能够被遍历一次，遍历一次之后，里面的数据就没有了
# lives_iterable = livebk.live.find().sort("date", self.MONGO_DESCENDING)
# # 鉴于lives_iterable数据类型的特殊性，这里用一个列表来缓存从lives_iterable里面的数据
#         lives = []
#         user_dict = {}
#         # 这里遍历lives_iterable,并把lives_iterable里面的数据全部缓存到lives列表里面去，修改其中的user属性，再传到前端。
#         for live in lives_iterable:
#             user_id = str(live["user"])
#             if user_id in user_dict.keys():
#                 user = user_dict[user_id]
#             else:
#                 user = livebk.user.find_one({"_id": ObjectId(user_id)})
#             lives.append({
#                 "photo_name": live["photo_name"],
#                 "user": user,
#                 "message": live["message"],
#                 "good": live["good"],
#                 "bad": live["bad"],
#                 "response": live["response"],
#                 "_id": live["_id"]
#             })
#
#         self.render("index.html", user=self.get_user(), lives=lives)


class IndexHandler(base.BaseLiveHandler):
    def get(self):
        db = self.get_livebk()
        lives_cursor = db.live.find().sort("date", self.MONGO_DESCENDING)
        lives = []

        for l in lives_cursor:
            live = self.wrapLive(l, db)
            lives.append(live)

        self.render("index.html", user=self.get_user(), lives=lives)


