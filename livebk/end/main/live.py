# coding=utf-8
__author__ = 'db'

import os.path
import re
import datetime

import tornado.web
from bson.objectid import ObjectId

import settings
import base


class SubmitHandler(base.BaseHandler):
    @tornado.web.authenticated
    def get(self, *args, **kwargs):
        self.render("submit.html", user=self.get_user())

    @tornado.web.authenticated
    def post(self, *args, **kwargs):
        message = self.get_argument("message")
        anonymous = self.get_argument("anonymous", False)
        photo = self.request.files["photo"]

        if anonymous != False:
            anonymous = True

        upload_path = os.path.join(settings.BASE_DIR, "front", "static", "photos")

        timestamp = re.compile("[\.:\- ]").sub("", str(datetime.datetime.now()))
        file_name = None
        file = None

        for meta in photo:
            file_name = meta['filename']
            file = meta["body"]

        photo_name = timestamp + "_" + file_name
        file_path = os.path.join(upload_path, photo_name)

        db = self.get_livebk()
        object_id = db.live.insert({
            "message": message,
            "anonymous": anonymous,
            "image": "/static/photos/" + photo_name,
            "user": self.get_user()["_id"],
            "date": datetime.datetime.now()
        })

        if object_id is not None:
            with open(file_path, 'wb') as up:
                up.write(file)
            self.redirect("/")


class LiveHandler(base.BaseLiveHandler):
    @tornado.web.authenticated
    def get(self, _id):
        _id = ObjectId(_id)
        db = self.get_livebk()
        live_cursor = db.live.find_one({"_id": _id})
        live = self.wrapLive(live_cursor, db)

        response_list = []
        response_list_cursor = db.comment.find({"live": _id, "type": 2}).sort("date", self.MONGO_ASCENDING)
        for response in response_list_cursor:
            for k in response.keys():
                if k == "user":
                    response[k] = db.user.find_one({"_id": response[k]})
            response_list.append(response)

        self.render("live.html", user=self.get_user(), live=live, response_list=response_list)


# class CommentHandler(base.BaseHandler):
# @tornado.web.authenticated
# def post(self):
# GOOD = "1"
# BAD = "0"
#
# comment = self.get_argument("comment")
# id = self.get_argument("id")
#
# db = self.get_livebk()
# live = db.live
#
# live_cursor = live.find_one({"_id": ObjectId(id)})
#
# user_id = self.get_user()["_id"]
#
# if live_cursor is not None:
#
# if user_id in live_cursor["good"] or user_id in live_cursor["bad"]:
# print "您已经评论过了"
# self.write({"success": False, "msg": "您已经评论过了"})
# return
# else:
# if comment == GOOD:
# # 注意：live_cursor["good"].append(username)的返回值是None。 --! shit！这里卡了我一个多小时！
# live_cursor["good"].append(user_id)
#                     live.update({"_id": ObjectId(id)}, {"$set": {"good": live_cursor["good"]}})
#                     self.write({
#                         "success": True,
#                         "msg": "评论成功",
#                         "data": {
#                             "count": len(live_cursor["good"])
#                         }
#                     })
#
#                 elif comment == BAD:
#                     live_cursor["bad"].append(user_id)
#                     live.update({"_id": ObjectId(id)}, {"$set": {"bad": live_cursor["bad"]}})
#                     self.write({
#                         "success": True,
#                         "msg": "评论成功",
#                         "data": {
#                             "count": len(live_cursor["bad"])
#                         }
#                     })
#                 else:
#                     self.write({"success": False, "msg": "评论参数非法"})
#                     return
#         else:
#             print "id不存在"

class CommentHandler(base.BaseHandler):
    @tornado.web.authenticated
    def post(self):
        _type = int(self.get_argument("type"))
        _id = self.get_argument("id")
        response = self.get_argument("response", "")

        _live_id = ObjectId(_id)
        _user_id = self.get_user()["_id"]

        db = self.get_livebk()
        if _type == 0:
            if db.comment.find_one({"type": _type, "live": _live_id}) is not None:
                self.write({
                    "success": False,
                    "msg": "您已经拍砖过了"
                })
            else:
                db.comment.insert({
                    "type": _type,
                    "live": _live_id,
                    "user": _user_id,
                    "date": self.now()
                })
                self.write({
                    "success": True,
                    "data": {
                        "count": db.comment.find({"live": _live_id, "type": _type}).count()
                    }
                })
        elif _type == 1:
            if db.comment.find_one({"type": _type, "live": _live_id}) is not None:
                self.write({
                    "success": False,
                    "msg": "您已经赞过了"
                })
            else:
                db.comment.insert({
                    "type": _type,
                    "live": _live_id,
                    "user": _user_id,
                    "date": self.now()
                })
                self.write({
                    "success": True,
                    "data": {
                        "count": db.comment.find({"live": _live_id, "type": _type}).count()
                    }
                })
        elif _type == 2:
            if response == "":
                self.write({
                    "success": False,
                    "msg": "评论文字不能为空"
                })
            elif len(response) > 200:
                self.write({
                    "success": False,
                    "msg": "评论文字不能多余200个字"
                })
            else:
                object_id = db.comment.insert({
                    "type": _type,
                    "live": _live_id,
                    "user": _user_id,
                    "response": response,
                    "date": self.now()
                })

                comment_cursor = db.comment.find_one({"_id": object_id})
                comment = {}
                comment["user"] = db.user.find_one({"_id": comment_cursor["user"]})
                comment["response"] = comment_cursor["date"]
                for k in comment_cursor.keys():
                    if k == "user":
                        comment[k] = db.user.find_one({"_id": comment_cursor[k]})
                    elif k == "date":
                        comment[k] = str(comment_cursor[k])
                    else:
                        comment[k] = comment_cursor[k]

                self.write({
                    "success": True,
                    "data": {
                        "comment": self.get_json(comment),
                        "count": db.comment.find({"live": _live_id, "type": 2}).count()
                    }
                })


class ResponseHandler(base.BaseHandler):
    def get(self, _id):
        db = self.get_livebk()
        live_cursor = db.live.find_one({"_id": ObjectId(_id)})
        live = {}
        for p in live_cursor:
            if p == "user":
                live[p] = db.user.find_one({"_id": live_cursor[p]})
            elif p == "good":
                good = live_cursor[p]
                live["good"] = []
                for g in good:
                    live["good"].append(db.good.find_one({"_id": g}))
            elif p == "bad":
                bad = live_cursor[p]
                live["bad"] = []
                for b in bad:
                    live["bad"].append(db.bad.find_one({"_id": b}))
            elif p == "response":
                response = live_cursor[p]
                live["response"] = []
                for r in response:
                    this_response = db.response.find_one({"_id": r})
                    live["response"].append({
                        "user": db.user.find_one({"_id": this_response["user"]}),
                        "response": this_response["response"]
                    })
            else:
                live[p] = live_cursor[p]

        self.render("response.html", user=self.get_user(), live=live)

    @tornado.web.authenticated
    def post(self, _id):
        response = self.get_argument("response", "")
        if response == "":
            self.write("回复内容不能为空！")
        elif len(response) > 240:
            self.write("回复内容长度不能超过240个字！")
        else:
            db = self.get_livebk()
            live = db.live
            live_cursor = live.find_one({"_id": ObjectId(_id)})
            # live_cursor["response"].append({"user": self.get_user(), "response": response})
            # live.update({"_id": ObjectId(_id)}, {"$set": {"response": live_cursor["response"]}})
            response_cursor = db.response
            response_id = response_cursor.insert({
                "response": response,
                "user": self.get_user()["_id"],
                "live": ObjectId(_id),
                "date": datetime.datetime.now()
            })

            live_cursor["response"].append(response_id)
            live.update({"_id": ObjectId(_id)}, {"$set": {"response": live_cursor["response"]}})

            if response_id is not None:
                self.redirect("/response/" + _id)
