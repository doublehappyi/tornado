__author__ = 'db'
# coding=utf-8
import urls
import settings
import tornado.ioloop
import tornado.web

if __name__ == "__main__":
    app = tornado.web.Application(urls.urls, **settings.settings)
    app.listen(8080)
    tornado.ioloop.IOLoop.instance().start()