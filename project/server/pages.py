__author__ = 'db'
import tornado.web
import tornado.gen
import tornado.httpclient
class IndexPage(tornado.web.RequestHandler):
    def initialize(self):
        print "initialize in IndexPage"

    def prepare(self):
        print "prepare in IndexPage"
        self.name = self.get_query_argument("name", "hello")
        print "self.get_query_arguments(name): ", self.get_query_arguments("name")
    def get(self, *args, **kwargs):
        print self.request.headers
        # self.set_cookie("name","yisx")
        #
        # self.write(self.request.headers )
        # self.write("\n\n")
        # self.write(self.get_cookie("name"))
        # self.write("\n\n")
        #self.write(self.cookies)
        # self.write({"name":"yisx"})
        # self.set_header("Content-Type","text/html")
        return self.render("index.html", name=self.name)

    def post(self, *args, **kwargs):
        print 'self.get_body_arguments ', self.get_body_argument("name")
        self.write(self.get_body_argument("name"))
    def on_finish(self):
        print "on_finish in IndexPage"


class GenPage(tornado.web.RequestHandler):
    @tornado.gen.coroutine
    def get(self, *args, **kwargs):
        async = tornado.httpclient.AsyncHTTPClient()
        response = yield async.fetch("http://item.jd.com/1217499.html")
        print response.body
        self.write("response")
        self.write(response.body)
        self.finish()
