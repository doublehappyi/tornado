__author__ = 'db'
import os.path

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

settings = {
    "template_path": os.path.join(BASE_DIR, "web", "templates"),
    "static_path": os.path.join(BASE_DIR, "web", "static"),
    "debug": True,
    "cookie_secret": "EOFsnUQ/SeG2V39seKNw0hwYJSevqEjxs/+lMFfHSd4=",
    "xsrf_cookies": False,
    "login_url": "/login"

}