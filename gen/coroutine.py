from tornado import gen
from tornado.httpclient import AsyncHTTPClient

@gen.coroutine
def fetch_coroutine(url):
  http_client = AsyncHTTPClient()
  res = yield http_client.fetch(url)
  raise gen.Return(res.body)

print "before fetch..."
c = fetch_coroutine("http://www.baidu.com")
print c
print "after fetch ..."

